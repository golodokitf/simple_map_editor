# simple_map_editor

Application features:
- Dynamic creation of new texture objects through a config file (imagePath field is required). Using the countLimit value you can limit the number of elements created
- Modify the already exposed elements. Their properties through the side panel after selecting an element.
- Saving
- Loading


**Build with:**

1. _Qt Creator_ **10.0.0**
1. _Qt_ **6.5.0 и выше**
1. **C++20**

![](map_editor.gif)