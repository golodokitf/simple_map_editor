#include "scene_model.h"

#include "config_controller.h"

#include "FileManager/file_manager.h"
#include "FlyWeight/fly_weigh_factory.h"
#include "FlyWeight/fly_weight.h"

#include "Common/constant.h"
#include "Common/flags.h"

#include "model_roles.h"

#include <QFile>
#include <QJsonObject>
#include <QQmlEngine>

constexpr size_t COLUMN = 30;
constexpr size_t ROWS = 15;

SceneModel::SceneModel(QObject *pParent)
    : QAbstractItemModel(pParent)
{
    declareQML();

    size_t count = COLUMN * ROWS;
    _data.reserve(count);

    while (count) {
        _data.push_back({});
        --count;
    }
}

void SceneModel::declareQML() // not goot place
{
    qmlRegisterType<Flags>("Flags", 1, 0, "Flags");
}

void SceneModel::createTile(size_t pos, size_t configIdx)
{
    if (pos >= _data.size())
        return;

    if (!checLimit(configIdx))
        return;

    auto config = ConfigController::instance().configByIdx(configIdx);

    if (!config.isValid())
        return;

    auto &elem = _data[pos];

    elem.setFlyWeight(FlyWeightFactory::instance().getFlyweight(config));
    elem.setPos(pos);

    ++_createdElementsCount[configIdx];

    if (!checLimit(configIdx)) {
        emit limitExceededChanged(configIdx, true);
    }

    emit dataChanged(index(pos, 0), index(pos, 0));
}

void SceneModel::moveTile(size_t from, size_t to)
{
    if (from >= _data.size() || to >= _data.size())
        return;

    auto &elemFrom = _data[from];
    auto &elemTo = _data[to];

    elemTo.setFlyWeight(elemFrom.getFlyWeight());
    elemTo.setPos(to);
    elemFrom.reset();

    emit dataChanged(index(std::min(from, to), 0), index(std::max(from, to), 0));
}

QJsonArray SceneModel::dataForSave() const
{
    QJsonArray result;

    for (const auto &item : _data) {
        if (!item.getPos().has_value())
            continue;

        QJsonObject jsObject;

        jsObject.insert(kElementPosition, QString::number(item.getPos().value_or(0)));
        jsObject.insert(kConfigId, QString::number(item.getConfigId().value_or(0)));

        result.push_back(jsObject);
    }

    return result;
}

void SceneModel::dataForRestore(const QJsonArray &jsArray)
{
    beginResetModel();

    for (auto &item : _data) {
        item.reset();
    }

    _createdElementsCount.clear();
    endResetModel();

    for (const auto &jsData : jsArray) {
        restoreItem(jsData.toObject());
    }
}

void SceneModel::restoreItem(const QJsonObject &jsData)
{
    const size_t configId = jsData.value(kConfigId).toString().toULongLong();
    const size_t pos = jsData.value(kElementPosition).toString().toULongLong();

    createTile(pos, configId);
}

bool SceneModel::checLimit(unsigned int configIdx)
{
    auto config = ConfigController::instance().configByIdx(configIdx);

    if (!config.isValid()) {
        return false;
    }

    const std::optional<size_t> limit = config.countLimit();

    if (!limit.value_or(0)) {
        return true;
    }

    return limit > _createdElementsCount[configIdx];
}

void SceneModel::deletTile(const QJsonObject &jsObject)
{
    if (jsObject.isEmpty()) {
        return;
    }
    const size_t from = jsObject.value("from").toInt();

    if (from > _data.size()) {
        return;
    }
    auto &item = _data[from];

    const size_t configId = item.getConfigId().value_or(0);

    --_createdElementsCount[configId];
    item.reset();

    emit limitExceededChanged(configId, !checLimit(configId));
    emit dataChanged(index(from, 0), index(from, 0));
}

void SceneModel::saveMap(const QVariant &vData)
{
    FileManager::saveMap(vData.toUrl(), ConfigController::instance().dataForSave(), dataForSave());
}

void SceneModel::loadMap(const QVariant &vData)
{
    QJsonObject jsObject = FileManager::loadMap(vData.toUrl());
    ConfigController::instance().dataForRestore(jsObject.value(kConfigs).toArray());
    dataForRestore(jsObject.value(kElements).toArray());
}

QJsonArray SceneModel::dataForModifyConfig(unsigned int tileId)
{
    QJsonArray result;

    if (tileId >= _data.size()) {
        return result;
    }
    auto configData = ConfigController::instance()
                          .configByIdx(_data.at(tileId).getConfigId().value_or(0))
                          .data();

    for (const auto &key : configData.keys()) {
        QJsonObject jsObject({{key, configData.value(key)}});
        result.push_back(jsObject);
    }

    return result;
}

void SceneModel::applyNewConfig(const QJsonArray &jsArray, unsigned int tileIndex)
{
    if (tileIndex >= _data.size()) {
        return;
    }
    QJsonObject jsObject;

    for (const auto &item : jsArray) {
        const auto tmpObject = item.toObject();

        const auto keys = tmpObject.keys();

        jsObject.insert(*keys.begin(), tmpObject.value(*keys.begin()));
    }

    std::optional<Config> config = ConfigController::instance().addConfig(jsObject, {}, true);

    if (!config.has_value()) {
        config = ConfigController::instance().configByData(jsObject);
    }
    if (!config->isCopied()) {
        return;
    }
    auto &item = _data[tileIndex];
    item.setFlyWeight(FlyWeightFactory::instance().getFlyweight(*config));
    emit dataChanged(index(tileIndex, 0), index(tileIndex, 0));
}

void SceneModel::newTile(const QJsonObject &jsObject)
{
    if (jsObject.isEmpty()) {
        return;
    }
    const Flags::CreationMod mode = jsObject.value("mode").toVariant().value<Flags::CreationMod>();
    const size_t from = jsObject.value("from").toInt();
    const size_t to = jsObject.value("to").toInt();

    switch (mode) {
    case Flags::CreationMod::enNew:
        createTile(to, from);
        break;
    case Flags::CreationMod::enMove:
        moveTile(from, to);
        break;
    }
}

QModelIndex SceneModel::index(int row, int column, const QModelIndex &parent) const
{
    if (column != 0 || parent.isValid()) {
        return QModelIndex();
    }
    if (row < 0 || row >= (int) _data.size()) {
        return QModelIndex();
    }
    return createIndex(row, column);
}

QModelIndex SceneModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int SceneModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return _data.size();
}

int SceneModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return 1;
}

QVariant SceneModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= (int) _data.size() || index.column() > 0) {
        return QVariant();
    }
    const auto &elem = _data.at(index.row());

    if (role == (int) SceneModelRole::enPathToImage) {
        if (auto spFlyWeight = elem.getFlyWeight()) {
            return spFlyWeight->imagePath();
        }
        return QString();
    } else if (role == (int) SceneModelRole::enTileInit) {
        return static_cast<bool>(elem.getFlyWeight());
    }

    return QVariant();
}

QHash<int, QByteArray> SceneModel::roleNames() const
{
    return {{(int) SceneModelRole::enPathToImage, "imagePath"},
            {(int) SceneModelRole::enTileInit, "tileInit"}};
}
