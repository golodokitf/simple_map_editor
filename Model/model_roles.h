#pragma once

#include <qnamespace.h>

enum class SceneModelRole
{
    enPathToImage = Qt::UserRole + 1,
    enTileInit,
};

enum class CreateButtonModelRole
{
    enPathToImage = Qt::UserRole + 1,
    enItemEnable,
    enConfigId,
};
