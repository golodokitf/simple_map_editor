#pragma once

#include <QAbstractItemModel>
#include <QJsonArray>
#include <unordered_map>

#include "BasicElements/base_element.h"

class QJsonObject;

class SceneModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    SceneModel(QObject* pParent = nullptr);

    Q_INVOKABLE void newTile(const QJsonObject&);
    Q_INVOKABLE bool checLimit(unsigned int configIdx);
    Q_INVOKABLE void deletTile(const QJsonObject&);

    Q_INVOKABLE void saveMap(const QVariant &);
    Q_INVOKABLE void loadMap(const QVariant &);
    Q_INVOKABLE QJsonArray dataForModifyConfig(unsigned int configId);
    Q_INVOKABLE void applyNewConfig(const QJsonArray& jsArray, unsigned int tileIndex);

signals:
    void limitExceededChanged(size_t configId, bool exceeded);

protected:
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;


private:
    void declareQML();
    void createTile(size_t pos, size_t configIdx);
    void moveTile(size_t from, size_t to);
    QJsonArray dataForSave() const;
    void dataForRestore(const QJsonArray& jsArray);
    void restoreItem(const QJsonObject& jsData);

private:
    std::vector<BaseElement> _data;
    std::unordered_map<size_t, size_t> _createdElementsCount;

};


