#pragma once
#include <QAbstractListModel>

#include "BasicElements/config.h"

class CreateButtonModel : public QAbstractListModel
{
    Q_OBJECT
public:
    CreateButtonModel(QObject* pParent = nullptr);
    Q_INVOKABLE void addNewConfig(const QVariant &);

public slots:
    void onLimitExceededChanged_new(size_t configId, bool exceeded);

protected:

    int columnCount(const QModelIndex &parent) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QHash<int,QByteArray> roleNames() const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private slots:
    void onConfigAdded_new(const Config &);
    void onClearData_new();
    void onFinishedRestore_new();

private:
    void init();

private:
    std::vector<Config> _data;
};
