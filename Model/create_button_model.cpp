#include "create_button_model.h"

#include "config_controller.h"

#include "model_roles.h"

#include "FileManager/file_manager.h"

CreateButtonModel::CreateButtonModel(QObject *pParent /* = nullptr*/)
    : QAbstractListModel(pParent)
{
    init();
    connect(&ConfigController::instance(),
            &ConfigController::configAdded,
            this,
            &CreateButtonModel::onConfigAdded_new);
    connect(&ConfigController::instance(),
            &ConfigController::clearData,
            this,
            &CreateButtonModel::onClearData_new);
    connect(&ConfigController::instance(),
            &ConfigController::finishedRestore,
            this,
            &CreateButtonModel::onFinishedRestore_new);
}

void CreateButtonModel::addNewConfig(const QVariant &list)
{
    for (const auto &item : list.value<QList<QUrl>>()) {
        ConfigController::instance().addConfig(FileManager::loadConfig(item));
    }
}

void CreateButtonModel::onLimitExceededChanged_new(size_t configId, bool exceeded)
{
    auto it = std::lower_bound(std::begin(_data),
                               std::end(_data),
                               configId,
                               [](const Config &config, size_t configId) {
                                   return config.getId() < configId;
                               });

    if (it != std::end(_data)) {
        it->setLimitExceeded(exceeded);

        size_t updateIndex = std::distance(std::begin(_data), it);

        emit dataChanged(index(updateIndex, 0), index(updateIndex, 0));
    }
}

int CreateButtonModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return 1;
}

int CreateButtonModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return ConfigController::instance().configs().size();
}

QHash<int, QByteArray> CreateButtonModel::roleNames() const
{
    return {{(int) CreateButtonModelRole::enPathToImage, "imagePath"},
            {(int) CreateButtonModelRole::enItemEnable, "itemEnable"},
            {(int) CreateButtonModelRole::enConfigId, "configId"}};
}

QVariant CreateButtonModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= (int) _data.size() || index.column() > 0) {
        return QVariant();
    }
    auto config = _data.at(index.row());

    if (role == (int) CreateButtonModelRole::enPathToImage) {
        return config.imagePath();
    } else if (role == (int) CreateButtonModelRole::enItemEnable) {
        return config.limitExceeded();
    } else if (role == (int) CreateButtonModelRole::enConfigId) {
        return QVariant::fromValue(config.getId());
    }

    return QVariant();
}

void CreateButtonModel::onConfigAdded_new(const Config &config)
{
    if (config.isCopied()) {
        return;
    }
    const size_t pos = _data.size();

    beginInsertRows({}, pos, pos);
    _data.push_back(config);
    endInsertRows();
}

void CreateButtonModel::onClearData_new()
{
    beginResetModel();
    _data.clear();
    endResetModel();
}

void CreateButtonModel::onFinishedRestore_new()
{
    std::sort(std::begin(_data), std::end(_data), [](const Config &left, const Config &right) {
        return left.getId() < right.getId();
    });
}

void CreateButtonModel::init()
{
    for (const auto &[key, config] : ConfigController::instance().configs()) {
        if (config.isCopied()) {
            continue;
        }
        _data.push_back(config);
    }

    std::sort(std::begin(_data), std::end(_data), [](const Config &left, const Config &right) {
        return left.getId() < right.getId();
    });
}
