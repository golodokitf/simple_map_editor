import QtQuick
import QtQuick.Controls
import SimpleMapEditor
import Qt.labs.platform

import SimpleMapEditorContext

ApplicationWindow  {
    id: _window
    visible: true
    maximumWidth: 1800
    minimumWidth: 1800
    maximumHeight: 1010
    width: 1800
    height: 1010

    title: qsTr("Map editor")

    FileDialog {
        id: _fileDialog
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
    }

    menuBar: MapMenuBar{}

    Connections
    {
        id: _saveMap
        target: _fileDialog
        enabled: false
        function onAccepted() {
            _saveMap.enabled = false
            SimpleMapEditor.sceneModel.saveMap(_fileDialog.file);
        }
    }

    Connections
    {
        id: _loadMap
        target: _fileDialog
        enabled: false
        function onAccepted() {
            _loadMap.enabled = false
            SimpleMapEditor.sceneModel.loadMap(_fileDialog.file);
        }
    }

    Scene
    {
        id: _scene
        anchors.fill: parent;
    }

    ConfigurationWindow
    {
        id: _configurationWindow
        width: 300
        height: parent.height - ConstantsValue.fotterHeight
        x: parent.width - _configurationWindow.width
        visible: false
        anchors.top: parent.top

    }
}
