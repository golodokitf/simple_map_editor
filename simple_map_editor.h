#pragma once

#include <QObject>

class SceneModel;
class CreateButtonModel;

class SimpleMapEditor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SceneModel *sceneModel READ sceneModel CONSTANT)
    Q_PROPERTY(CreateButtonModel *createButtonModel READ createButtonModel CONSTANT)
public:
    explicit SimpleMapEditor(QObject *parent = nullptr);

private:
    SceneModel *sceneModel() const;
    CreateButtonModel *createButtonModel() const;

private:
    SceneModel *_sceneModel;
    CreateButtonModel *_createButtonModel;
};
