#pragma once

#include <memory>
#include <optional>

class FlyWeight;

class BaseElement
{
public:
    BaseElement();
    void setFlyWeight(const std::shared_ptr<FlyWeight>& spFlyWeight);
    std::shared_ptr<FlyWeight> getFlyWeight() const;

    void setPos(size_t pos);
    std::optional<size_t> getPos() const;

    std::optional<size_t> getConfigId() const;

    void assign(const BaseElement& elem);
    void reset();

private:
    std::optional<size_t> _pos;
    std::weak_ptr<FlyWeight> _wkData;
};

