#include "config.h"

#include <Common/constant.h>

Config::Config()
    : _limitExceeded(false)
    , _isCopied(false)
{}

Config::Config(size_t id, const QJsonObject &data)
    : _id(id)
    , _data(data)
    , _limitExceeded(false)
    , _isCopied(false)
{}

size_t Config::getId() const
{
    return _id.value_or(0);
}

QString Config::imagePath() const
{
    return _data.value(kImagePath).toString();
}

QJsonObject Config::data() const
{
    return _data;
}

std::optional<size_t> Config::countLimit() const
{
    std::optional<size_t> result;
    auto it = _data.find(kCountLimit);

    if (it != std::end(_data))
        result = it->toInt(0);

    return result;
}

bool Config::limitExceeded() const
{
    return _limitExceeded;
}

bool Config::isCopied() const
{
    return _isCopied;
}

void Config::setLimitExceeded(bool isLimitExceeded)
{
    _limitExceeded = isLimitExceeded;
}

void Config::setIsCopied(bool isCopied)
{
    _isCopied = isCopied;
}

bool Config::operator==(const Config &data)
{
    return _id == data._id && _data == data.data();
}

bool Config::operator==(size_t idx)
{
    return _id == idx;
}

bool Config::isValid() const
{
    return _id.has_value();
}
