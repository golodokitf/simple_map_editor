#pragma once

#include <QJsonObject>

class Config
{
public:
    Config();
    Config(size_t id, const QJsonObject& data);

    size_t getId() const;
    QString imagePath() const;
    QJsonObject data() const;
    std::optional<size_t> countLimit() const;
    bool limitExceeded() const; //not good way
    bool isCopied() const;

    void setLimitExceeded(bool isLimitExceeded);
    void setIsCopied(bool isCopied);

    bool operator==(const Config&);
    bool operator==(size_t idx);

    bool isValid() const;

private:
    std::optional<size_t> _id;
    QJsonObject _data;
    bool _limitExceeded;
    bool _isCopied;
};
