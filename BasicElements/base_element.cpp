#include "base_element.h"

#include "FlyWeight/fly_weight.h"

BaseElement::BaseElement() {}

void BaseElement::setFlyWeight(const std::shared_ptr<FlyWeight> &spFlyWeight)
{
    _wkData = spFlyWeight;
}

std::shared_ptr<FlyWeight> BaseElement::getFlyWeight() const
{
    if (auto spData = _wkData.lock())
        return spData;

    return nullptr;
}

void BaseElement::setPos(size_t pos)
{
    _pos = pos;
}

std::optional<size_t> BaseElement::getPos() const
{
    return _pos;
}

std::optional<size_t> BaseElement::getConfigId() const
{
    if (auto spData = _wkData.lock())
        return spData->configId();

    return {};
}

void BaseElement::assign(const BaseElement &elem)
{
    _pos = elem._pos;
    _wkData = elem._wkData.lock();
}

void BaseElement::reset()
{
    _pos.reset();
    _wkData.reset();
}
