
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "simple_map_editor.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/Main.qml"_qs);
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);

    SimpleMapEditor SimpleMapEditor(&engine);

    engine.load(url);

    return app.exec();
}
