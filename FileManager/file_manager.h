#pragma once

#include <QObject>
#include <QUrl>

class QJsonObject;

class FileManager : public QObject
{
    Q_OBJECT
public:
    FileManager();

    static QJsonObject loadConfig(QUrl pathToFile);
    static void saveMap(QUrl pathToFile, const QJsonArray& jsConfigs, const QJsonArray& elements);
    static QJsonObject loadMap(QUrl pathToFile);

signals:
    void configDataLoaded(const QJsonObject&);

};

