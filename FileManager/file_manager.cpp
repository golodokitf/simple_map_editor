#include "file_manager.h"

#include "Common/constant.h"

#include "Tools/file_helper.h"

#include <QByteArray>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

FileManager::FileManager() {}

QJsonObject FileManager::loadConfig(QUrl pathToFile)
{
    QJsonObject result;

    QFile jsonFile(FileHelper::skipPrefixIfNecessary(pathToFile));
    if (jsonFile.open(QFile::ReadOnly)) {
        result = QJsonDocument().fromJson(jsonFile.readAll()).object();
        jsonFile.close();
    }

    return result;
}

void FileManager::saveMap(QUrl pathToFile, const QJsonArray &jsConfigs, const QJsonArray &elements)
{
    QFile file(FileHelper::skipPrefixIfNecessary(pathToFile));
    if (file.open(QFile::WriteOnly)) {
        QJsonObject jsObject;

        jsObject.insert(kConfigs, jsConfigs);
        jsObject.insert(kElements, elements);

        QJsonDocument doc(jsObject);

        file.write(doc.toJson());
        file.close();
    }
}

QJsonObject FileManager::loadMap(QUrl pathToFile)
{
    QJsonObject result;

    QFile file(FileHelper::skipPrefixIfNecessary(pathToFile));
    if (file.open(QFile::ReadOnly)) {
        QJsonDocument d = QJsonDocument::fromJson(file.readAll());
        file.close();
        result = d.object();
    }

    return result;
}
