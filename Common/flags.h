#pragma once
#include <QObject>

class Flags : public QObject
{
    Q_OBJECT
public:
    enum class CreationMod
    {
        enNew,
        enMove
    };

    Q_ENUM(CreationMod)
};

