#pragma once

namespace  {
static constexpr char const *kImagePath = "imagePath";
static constexpr char const *kCountLimit = "countLimit";
static constexpr char const *kConfigs = "configs";
static constexpr char const *kElements = "elements";
static constexpr char const *kElementPosition = "elementPosition";
static constexpr char const *kConfigId = "configId";
static constexpr char const *kConfigData = "configData";
static constexpr char const *kIsCopied = "isCopied";
}

