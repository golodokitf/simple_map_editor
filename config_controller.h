#pragma once

#include "BasicElements/config.h"

#include <QObject>
#include <QJsonArray>
#include <unordered_map>

class ConfigController : public QObject
{
    Q_OBJECT
public:
    static ConfigController& instance();

    std::optional<Config> configByData(const QJsonObject& jsObject);
    std::optional<Config> addConfig(const QJsonObject& jsObject, std::optional<size_t> configId = std::optional<size_t>(), bool isCopied = false);
    Config& configByIdx(size_t idx);
    std::unordered_map<size_t, Config>& configs();

    QJsonArray dataForSave() const;
    void dataForRestore(const QJsonArray& jsArray);

signals:
    void configAdded(const Config&);
    void clearData();
    void finishedRestore();

private:
    ConfigController();
    ConfigController(const ConfigController&)= delete;
    ConfigController& operator=(const ConfigController&)= delete;

    QString skipPrefixIfNecessary(const QUrl& url);

private:
    std::unordered_map<size_t, Config> m_configs;
};

