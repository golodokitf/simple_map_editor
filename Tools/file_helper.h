#pragma once
#include <QString>
#include <QUrl>
#include <QtGlobal>

class FileHelper{
public:
    inline static QString skipPrefixIfNecessary(const QUrl &url)
    {
        QString result = url.url();

#ifdef Q_OS_WIN
        result = result.replace("file:///", "");
#endif

#ifdef Q_OS_MACOS
        result = result.replace("file:///", "/");
#endif
        return result;
    }
};
