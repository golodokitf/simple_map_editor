#include "config_controller.h"

#include "Common/constant.h"
#include "FileManager/file_manager.h"

#include <QDir>
#include <QFile>
#include <QJsonDocument>

ConfigController &ConfigController::instance()
{
    static ConfigController singleton;
    return singleton;
}

std::optional<Config> ConfigController::configByData(const QJsonObject &jsObject)
{
    std::optional<Config> result;

    auto it = std::find_if(std::begin(m_configs), std::end(m_configs), [jsObject](const std::pair<size_t, Config>& tmpObj)
                           {
                               return  tmpObj.second.data() == jsObject;
                           });

    if (it != std::end(m_configs)) {
        result = it->second;
    }
    return  result;
}

Config& ConfigController::configByIdx(size_t idx)
{
    static Config nullObj;

    auto it = m_configs.find(idx);

    return it == std::end(m_configs) ? nullObj : it->second;
}

std::unordered_map<size_t, Config> &ConfigController::configs()
{
    return  m_configs;
}

QJsonArray ConfigController::dataForSave() const
{
    QJsonArray result;

    for(const auto&     [key, config] : m_configs)
    {
        QJsonObject jsObject;

        jsObject.insert(kConfigId, QString::number(config.getId()));
        jsObject.insert(kConfigData, config.data());
        jsObject.insert(kIsCopied, config.isCopied());

        result.push_back(jsObject);
    }

    return  result;
}

void ConfigController::dataForRestore(const QJsonArray &jsArray)
{
    m_configs.clear();

    emit clearData();

    for(const auto& jsData: jsArray){
        const auto& jsObject = jsData.toObject();
        addConfig(jsObject.value(kConfigData).toObject(), jsObject.value(kConfigId).toString().toULongLong(), jsObject.value(kIsCopied).toBool());
    }

    emit finishedRestore();

}

ConfigController::ConfigController()
{
    addConfig(FileManager::loadConfig(QDir::currentPath() + "/defaultConfig/wall.json"));
    addConfig(FileManager::loadConfig(QDir::currentPath() + "/defaultConfig/stairs.json"));
    addConfig(FileManager::loadConfig(QDir::currentPath() + "/defaultConfig/skater.json"));
}

std::optional<Config> ConfigController::addConfig(const QJsonObject &jsObject, std::optional<size_t> configId, bool isCopied)
{
    auto config = configByData(jsObject);

    if(config.has_value())
        return {};

    const size_t idx = configId.has_value() ? configId.value_or(0) : m_configs.size();

    config = Config(idx, jsObject);
    config->setIsCopied(isCopied);
    m_configs.insert({idx, *config});
    emit configAdded(*config);

    return config;
}


