
#include "simple_map_editor.h"

#include "Model/create_button_model.h"
#include "Model/scene_model.h"

#include <QQmlEngine>

SimpleMapEditor::SimpleMapEditor(QObject *parent)
    : QObject{parent}
    , _sceneModel(new SceneModel(this))
    , _createButtonModel(new CreateButtonModel(this))
{
    connect(_sceneModel,
            &SceneModel::limitExceededChanged,
            _createButtonModel,
            &CreateButtonModel::onLimitExceededChanged_new);

    qmlRegisterSingletonInstance("SimpleMapEditorContext", 1, 0, "SimpleMapEditor", this);
}

SceneModel *SimpleMapEditor::sceneModel() const
{
    return _sceneModel;
}

CreateButtonModel *SimpleMapEditor::createButtonModel() const
{
    return _createButtonModel;
}
