import QtQuick 2.12
import QtQuick.Controls 2.15

MenuBar {

    Menu {
        title: qsTr("&File")
        Action {
            text: qsTr("&Save...")
            onTriggered: {
                _saveMap.enabled = true
                _fileDialog.nameFilters = [ "All files (*.gsme)" ]
                _fileDialog.fileMode = 2
                _fileDialog.open();
            }


        }
        Action {
            text: qsTr("&Load...")
            onTriggered:
            {
                _loadMap.enabled = true
                _fileDialog.nameFilters = [ "All files (*.gsme)" ]
                _fileDialog.fileMode = 0
                _fileDialog.open();
            }


        }
    }
}
