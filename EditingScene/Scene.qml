import QtQuick 2.0
import QtQuick.Layouts 1.12

import SimpleMapEditorContext
import SimpleMapEditor

Rectangle {
    id: _scene

    ColumnLayout {
        spacing: 0
        anchors.fill: parent
        GridView {
             Layout.fillHeight: true
             Layout.fillWidth: true
            id: grid
            cellWidth: ConstantsValue.textureWidth
            cellHeight: ConstantsValue.textureHeight
            model: SimpleMapEditor.sceneModel

            delegate: SceneGridDelegate{}
        }

        Footer {
            id: _footer
            Layout.maximumHeight: 62
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }

}
