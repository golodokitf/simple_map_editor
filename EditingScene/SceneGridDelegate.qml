import QtQuick 2.0

import SimpleMapEditor
import SimpleMapEditorContext

Item {
    width: ConstantsValue.textureWidth
    height: ConstantsValue.textureHeight

    DropTile {
        id: _gripDropTile
        visible: !tileInit
        onDropped : (drop) => {
                        drop.acceptProposedAction();
                        var tmpJs = JSON.parse(drop.getDataAsArrayBuffer("text/plain"));
                        tmpJs["to"] = index;
                        SimpleMapEditor.sceneModel.newTile(tmpJs);
                    }
    }
    DragTile {
        id: _gripDragTile
        image: imagePath
        visible: tileInit
        onDragTileClicked: {

            _configurationWindow.visible = true;

            if(_configurationWindow.visible){
                _configurationWindow.model = SimpleMapEditor.sceneModel.dataForModifyConfig(index);
                _configurationWindow.tileIndex = index;
            }
        }
    }
}
