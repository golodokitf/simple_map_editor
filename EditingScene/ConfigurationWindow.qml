import QtQuick
import QtQuick.Controls

import SimpleMapEditor
import SimpleMapEditorContext

Rectangle {
    id: _configurationWindow
    color: "#DCDCDC"

    property var model
    property int tileIndex

    function modifyData(newValue, key, index) {
        model[index][key] = newValue;
        _repeater.model = model;
    }

    onModelChanged: {
        _repeater.model = model;
    }

    Item {
        id: _actionButton
        height: 30
        anchors
        {
            top:  parent.top
            left: parent.left
            right: parent.right
        }

        Row {

            anchors.centerIn: parent
            spacing: 10
            RoundButton {

                onClicked: {
                    SimpleMapEditor.sceneModel.applyNewConfig(_repeater.model, tileIndex);
                    _configurationWindow.visible = false
                }

                background: Rectangle {
                    color: "#00b386"
                    anchors.fill: parent
                    radius: 10
                }

                text: "APPLY"
            }

            RoundButton {

                onClicked: _configurationWindow.visible = false

                background: Rectangle {
                    color: "#cc6600"
                    anchors.fill: parent
                    radius: 10
                }

                text: "CANSEL"
            }
        }
    }

    Flickable {
        id: _flickable
        clip: true

        contentHeight: _column.height

        anchors {
            top:  _actionButton.bottom
            topMargin: 10
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Column {
            id: _column
            anchors
            {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            spacing: 5
            Repeater
            {
                id: _repeater
                anchors.fill: parent
                model: _configurationWindow.model

                delegate: ConfigField
                {
                    width: parent.width
                    height: 65

                    onModifyData: _configurationWindow.modifyData(newValue, key, index)
                }
            }
        }
    }
}
