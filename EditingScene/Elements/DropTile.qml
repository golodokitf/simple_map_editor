import QtQuick

import SimpleMapEditor

DropArea {
    id: dragTarget

    width: ConstantsValue.textureWidth
    height: ConstantsValue.textureHeight
    keys: ["text/plain"]

    Rectangle {
        id: _dropRectangle
        color: "white"
        anchors.fill: parent
        anchors.margins: 1
        border.width: 1
        border.color: "#3d422f"

        Image {
            id: _dropTexture
            anchors.fill: parent
        }

        states: [
            State {
                when: dragTarget.containsDrag
                PropertyChanges {
                    target: _dropRectangle
                    color: "grey"
                }
            }
        ]
    }
}
