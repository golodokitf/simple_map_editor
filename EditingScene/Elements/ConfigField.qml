import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt.labs.platform

Rectangle {

    id: _configField

    signal modifyData(var newValue, var key, int index)

    radius: 10

    function getKey(data) {
        return Object.keys(data)[0];
    }

    function getData (data) {
        return Object.values(data)[0];
    }

    Text {
        id: _configKey
        text: getKey(modelData) + ":"
        width: contentWidth
        anchors {
            left: parent.left
            right: _chooseImg.visible ? _chooseImg.left : parent.right
            top: parent.top
            topMargin: 5
        }
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    RoundButton {
        id: _chooseImg
        anchors {
            right: parent.right
            rightMargin: 4
            top: parent.top
            topMargin: 4
        }
        width: 110
        height: 20
        font.pixelSize : 10
        background: Rectangle {
            color: "#C0C0C0"
            anchors.fill: parent
            radius: 10
        }

        text: "CHOSEN PATH"

        onClicked: () => {
            _pathСhosenConnection.enabled = true
            _fileDialog.nameFilters = [ "Image files (*.jpg *.png *.svg) All files (*.png *.svg *.jpg)" ]
            _fileDialog.fileMode = FileDialog.OpenFile
            _fileDialog.open();
        }

        Connections {
            id: _pathСhosenConnection
            target: _fileDialog
            enabled: false
            function onAccepted() {
                _pathСhosenConnection.enabled = false;
                _textField.text = _fileDialog.file.toString().replace("file:///", "");
                _configField.modifyData(_textField.text, getKey(modelData), index);
            }
        }

        visible: getKey(modelData) === "imagePath"

    }

    TextField {
        id: _textField
        anchors{
            left: parent.left
            right: parent.right
            top: _configKey.bottom
            topMargin: 7
        }
        selectByMouse: true
        width: 100
        text: getData(modelData)
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLefts

        onEditingFinished: () => {
            _configField.modifyData(_textField.text, getKey(modelData), index);
        }

    }
}

