import QtQuick

DropArea {
    id: _trashCan

    signal hitTheTrash(var data)

    keys: ["text/plain"]

    onDropped: (drop) => {
                    drop.acceptProposedAction();
                   _trashCan.hitTheTrash(JSON.parse(drop.getDataAsArrayBuffer("text/plain")));
               }


    Rectangle {
        id: _dropRectangle
        color: "transparent"
        anchors.fill: parent

        Image {
            id: _dropTexture
            anchors.fill: parent
            source: "qrc:/img/trash.svg"
        }

        states: [
            State {
                when: _trashCan.containsDrag
                PropertyChanges {
                    target: _dropRectangle
                    color: "#cc6600"
                }
            }
        ]
    }

}
