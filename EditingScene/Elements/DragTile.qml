import QtQuick
import Flags
import SimpleMapEditor

Item {
    id: _dragTile

    property string image
    property var mode: Flags.CreationMod.enMove
    property int from: -1

    signal dragTileClicked();

    width: ConstantsValue.textureWidth;
    height: ConstantsValue.textureHeight

    function jsonData() {
        return JSON.stringify({from: from === -1 ? index : from, mode: mode});
    }

    function imagePathConversion() {
        if(!image.length)
            return ""

        return image.search("qrc") == 0 ? image : "file:///" + image;
    }

    MouseArea {
        id: _dragMouseArea

        width: ConstantsValue.textureHeight;
        height: ConstantsValue.textureHeight
        anchors.centerIn: parent

        onClicked: _dragTile.dragTileClicked();
        onReleased: parent = _dragTile

        drag.target: tile

        Rectangle {
            id: tile
            color: "white"
            width: _dragMouseArea.width;
            height: _dragMouseArea.height

            Drag.active: _dragMouseArea.drag.active
            Drag.hotSpot.x: 30
            Drag.hotSpot.y: 30
            Drag.dragType: Drag.Automatic
            Drag.mimeData: { "text/plain": jsonData()}
            Drag.imageSource: imagePathConversion()
            Image {
                id: _textures
                source:  imagePathConversion();
                anchors.fill: parent
            }
            states: State {
                when: _dragMouseArea.drag.active
                ParentChange { target: tile; parent: _scene }
            }

        }
    }
}


