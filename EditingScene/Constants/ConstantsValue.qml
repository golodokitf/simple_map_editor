pragma Singleton

import QtQuick 2.12

QtObject {
    id: constantsValue

    readonly property int textureWidth: 60
    readonly property int textureHeight: 60
    readonly property int fotterHeight: 62
}
