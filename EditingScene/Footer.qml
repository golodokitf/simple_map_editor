import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import Qt.labs.platform

import Flags
import SimpleMapEditor
import SimpleMapEditorContext

Rectangle {
    id: _footer
    color: "white"
    signal  createTile(var imagePath, var pos);

    Rectangle {
        id: _topBorder
        height: 1
        color: "black"
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
    }

    RowLayout {
        anchors.fill: parent

        ListView {
            id:  _createButtonList
            orientation: ListView.Horizontal
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: SimpleMapEditor.createButtonModel
            interactive: true
            delegate: DragTile {
                id: _delegate
                image:  imagePath;
                mode: Flags.CreationMod.enNew
                from: configId
                anchors.bottom : parent != null ? parent.bottom : undefined
                enabled: itemEnable ? false : true
                opacity: itemEnable ? 0.5 : 1
            }

        }

        RoundButton {
            id: _addButton
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumHeight: 30
            Layout.maximumWidth:  120
            Layout.rightMargin: 10
            text: "Add New Texture"

            onClicked: {
                _addNewConfigConnect.enabled = true
                _fileDialog.nameFilters = [ "All files (*.json)" ]
                _fileDialog.fileMode = FileDialog.OpenFiles
                _fileDialog.open();
            }

            Connections {
                id: _addNewConfigConnect
                target: _fileDialog
                enabled: false
                function onAccepted() {
                    _addNewConfigConnect.enabled = false
                    SimpleMapEditor.createButtonModel.addNewConfig(_fileDialog.files);
                }
            }
        }

        TrashCan {
            id: _trashCan
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumHeight: 40
            Layout.maximumWidth:  40
            Layout.rightMargin: 10
            onHitTheTrash: (data) => {  SimpleMapEditor.sceneModel.deletTile(data) }
        }
    }
}
