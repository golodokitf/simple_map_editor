#include "fly_weight.h"

FlyWeight::FlyWeight(const Config &data)
    : _data(data)
{}

bool FlyWeight::operator==(const FlyWeight &data)
{
    return _data == data._data;
}

QString FlyWeight::imagePath() const
{
    return _data.imagePath();
}

QJsonObject FlyWeight::data() const
{
    return _data.data();
}

size_t FlyWeight::configId() const
{
    return _data.getId();
}
