#pragma once

#include "BasicElements/config.h"

#include <QJsonObject>
#include <memory>

class FlyWeight
{
public:
    FlyWeight(const Config &data);

    bool operator==(const FlyWeight &);

    QString imagePath() const;
    QJsonObject data() const;
    size_t configId() const;

private:
    Config _data;
};

inline bool operator==(const std::shared_ptr<FlyWeight> &left,
                       const std::shared_ptr<FlyWeight> &right)
{
    return *left == *right;
}

namespace std {
inline uint qHash(const std::shared_ptr<FlyWeight> &key, uint seed)
{
    return qHash((*key).data(), seed);
}
} // namespace std
