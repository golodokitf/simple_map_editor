#pragma once

#include <memory>
#include <QSet>

class Config;
class FlyWeight;

class FlyWeightFactory
{
public:
    std::shared_ptr<FlyWeight> getFlyweight(const Config& data);
    static FlyWeightFactory& instance();
    void clear();

private:
    FlyWeightFactory();
    FlyWeightFactory(const FlyWeightFactory&)= delete;
    FlyWeightFactory& operator=(const FlyWeightFactory&)= delete;

private:
    QSet<std::shared_ptr<FlyWeight>> _chash;

};


