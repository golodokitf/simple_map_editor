#include "fly_weigh_factory.h"

#include "fly_weight.h"

std::shared_ptr<FlyWeight> FlyWeightFactory::getFlyweight(const Config &data)
{
    auto spData = std::make_shared<FlyWeight>(data);

    if (auto it = _chash.find(spData); it != std::end(_chash))
        return std::shared_ptr<FlyWeight>(*it);

    _chash.insert(spData);

    return spData;
}

FlyWeightFactory &FlyWeightFactory::instance()
{
    static FlyWeightFactory singleton;
    return singleton;
}

void FlyWeightFactory::clear()
{
    _chash.clear();
}

FlyWeightFactory::FlyWeightFactory() {}
